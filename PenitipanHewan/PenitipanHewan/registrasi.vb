﻿Imports MySql.Data.MySqlClient
Public Class registrasi

    Sub tampilRegistrasi()
        Call bukaDB()
        DA = New MySqlDataAdapter("SELECT id 'ID',username 'Username', password 'Password' from admin", Conn)
        DS = New DataSet
        DA.Fill(DS, "admin")
        DataGridView1.DataSource = DS.Tables("admin")
        DataGridView1.ReadOnly = True
    End Sub
    Sub autoNumber()
        Call bukaDB()
        CMD = New MySqlCommand("select * from admin order by id desc", Conn)
        RD = CMD.ExecuteReader
        RD.Read()
        If Not RD.HasRows Then
            TextBox1.Text = "1001"
        Else
            TextBox1.Text = Val(Microsoft.VisualBasic.Mid(RD.Item("id").ToString, 4, 3)) + 1
            If Len(TextBox1.Text) = 1 Then
                TextBox1.Text = "100" & TextBox1.Text & ""
            ElseIf Len(TextBox1.Text) = 2 Then
                TextBox1.Text = "10" & TextBox1.Text & ""
            ElseIf Len(TextBox1.Text) = 3 Then
                TextBox1.Text = "1" & TextBox1.Text & ""
            End If
        End If
    End Sub
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call autoNumber()
        Call tampilRegistrasi()
    End Sub
    Sub clearRegistrasi()
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox1.Focus()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Call bukaDB()
            CMD = New MySqlCommand("SELECT id from admin WHERE id = '" & TextBox1.Text & "'", Conn)
            RD = CMD.ExecuteReader
            RD.Read()
            If RD.HasRows Then
                MsgBox("Maaf Data dengan ID tersebut sudah ada", MsgBoxStyle.Exclamation, "peringatan")
            Else
                Call bukaDB()
                simpan = "INSERT INTO admin (id,username,password) VALUES (?,?,?)"
                CMD = Conn.CreateCommand
                With CMD
                    .CommandText = simpan
                    .Connection = Conn
                    .Parameters.Add("p1", MySqlDbType.Int16, 100).Value = TextBox1.Text
                    .Parameters.Add("p2", MySqlDbType.VarChar, 100).Value = TextBox2.Text
                    .Parameters.Add("p3", MySqlDbType.VarChar, 100).Value = TextBox3.Text
                    .ExecuteNonQuery()
                End With
                Call tampilRegistrasi()
                Call clearRegistrasi()
                Call autoNumber()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Terjadi kesalahan")
        End Try
    End Sub


    Private Sub LinkLabel3_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked
        halamanutama.Show()
        Me.Hide()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Call clearRegistrasi()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If DataGridView1.CurrentRow.Index <> DataGridView1.NewRowIndex Then
            Dim HapusDetail As String = "delete from admin where id=" &
            DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value & ""
            CMD = New MySql.Data.MySqlClient.MySqlCommand(HapusDetail, Conn)
            CMD.ExecuteNonQuery()
            DataGridView1.Rows.RemoveAt(DataGridView1.CurrentRow.Index)
            Call tampilRegistrasi()
            Call clearRegistrasi()
        End If
    End Sub


    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        TextBox1.Text = DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value
        TextBox2.Text = DataGridView1.Item(1, DataGridView1.CurrentRow.Index).Value
        TextBox3.Text = DataGridView1.Item(2, DataGridView1.CurrentRow.Index).Value
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        TextBox1.Text = DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value
        TextBox2.Text = DataGridView1.Item(1, DataGridView1.CurrentRow.Index).Value
        TextBox3.Text = DataGridView1.Item(2, DataGridView1.CurrentRow.Index).Value
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If DataGridView1.CurrentRow.Index <> DataGridView1.NewRowIndex Then
            Try
                Call bukaDB()
                ubah = "UPDATE admin SET id = '" & TextBox1.Text & "' ,username = '" & TextBox2.Text & "',password = '" & TextBox3.Text & "' where id = '" & TextBox1.Text & "'"
                CMD = New MySql.Data.MySqlClient.MySqlCommand(ubah, Conn)
                CMD.ExecuteNonQuery()
                Call tampilRegistrasi()
                Call clearRegistrasi()
                Call autoNumber()
            Catch ex As Exception
                MsgBox(ex.ToString, MsgBoxStyle.Critical, "Terjadi kesalahan")
            End Try
        End If
    End Sub
End Class
