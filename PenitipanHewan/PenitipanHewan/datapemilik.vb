﻿Imports MySql.Data.MySqlClient
Public Class datapemilik

    Private Property strRadioText As Object

    Sub tampilPemilik()
        Call bukaDB()
        DA = New MySqlDataAdapter("SELECT id_pem 'ID',nama 'Nama',alamat 'Alamat',nohp 'No HP', status 'Status' from pemilik", Conn)
        DS = New DataSet
        DA.Fill(DS, "pemilik")
        DataGridView1.DataSource = DS.Tables("pemilik")
        DataGridView1.ReadOnly = True
    End Sub
    Sub autoNumber()
        Call bukaDB()
        CMD = New MySqlCommand("select * from pemilik order by id_pem desc", Conn)
        RD = CMD.ExecuteReader
        RD.Read()
        If Not RD.HasRows Then
            TextBox1.Text = "2001"
        Else
            TextBox1.Text = Val(Microsoft.VisualBasic.Mid(RD.Item("id_pem").ToString, 4, 3)) + 1
            If Len(TextBox1.Text) = 1 Then
                TextBox1.Text = "200" & TextBox1.Text & ""
            ElseIf Len(TextBox1.Text) = 2 Then
                TextBox1.Text = "20" & TextBox1.Text & ""
            ElseIf Len(TextBox1.Text) = 3 Then
                TextBox1.Text = "2" & TextBox1.Text & ""
            End If
        End If
    End Sub
    Private Sub datapemilik_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call autoNumber()

        Call tampilPemilik()
    End Sub


    Sub clearPemilik()
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox1.Focus()
    End Sub
    
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Call bukaDB()
            CMD = New MySqlCommand("SELECT id_pem from pemilik WHERE id_pem = '" & TextBox1.Text & "'", Conn)
            RD = CMD.ExecuteReader
            RD.Read()
            If RD.HasRows Then
                MsgBox("Maaf Data dengan ID tersebut sudah ada", MsgBoxStyle.Exclamation, "peringatan")
            Else
                Call bukaDB()
                simpan = "INSERT INTO pemilik (id_pem,nama,alamat,nohp,status) VALUES (?,?,?,?,?)"
                CMD = Conn.CreateCommand
                Dim strRadioText As String
                If RadioButton1.Checked = True Then
                    strRadioText = RadioButton1.Text
                Else
                    strRadioText = RadioButton2.Text
                End If
                With CMD
                    .CommandText = simpan
                    .Connection = Conn
                    .Parameters.Add("p1", MySqlDbType.Int16, 100).Value = TextBox1.Text
                    .Parameters.Add("p2", MySqlDbType.VarChar, 100).Value = TextBox2.Text
                    .Parameters.Add("p3", MySqlDbType.VarChar, 100).Value = TextBox3.Text
                    .Parameters.Add("p4", MySqlDbType.VarChar, 100).Value = TextBox4.Text
                    .Parameters.Add("p5", MySqlDbType.VarChar, 100).Value = strRadioText
                    .ExecuteNonQuery()
                End With
                Call autoNumber()

                Call tampilPemilik()

                Call clearPemilik()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Terjadi kesalahan")
        End Try
    End Sub
   
    Private Sub LinkLabel3_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked
        halamanutama.Show()
        Me.Hide()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox1.Focus()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If DataGridView1.CurrentRow.Index <> DataGridView1.NewRowIndex Then
            Dim HapusDetail As String = "delete from pemilik where id_pem=" &
            DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value & ""
            CMD = New MySql.Data.MySqlClient.MySqlCommand(HapusDetail, Conn)
            CMD.ExecuteNonQuery()
            DataGridView1.Rows.RemoveAt(DataGridView1.CurrentRow.Index)
            Call autoNumber()

            Call tampilPemilik()

            Call clearPemilik()
        End If
    End Sub



    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        TextBox1.Text = DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value
        TextBox2.Text = DataGridView1.Item(1, DataGridView1.CurrentRow.Index).Value
        TextBox3.Text = DataGridView1.Item(2, DataGridView1.CurrentRow.Index).Value
        TextBox4.Text = DataGridView1.Item(3, DataGridView1.CurrentRow.Index).Value
        strRadioText = DataGridView1.Item(4, DataGridView1.CurrentRow.Index).Value
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        TextBox1.Text = DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value
        TextBox2.Text = DataGridView1.Item(1, DataGridView1.CurrentRow.Index).Value
        TextBox3.Text = DataGridView1.Item(2, DataGridView1.CurrentRow.Index).Value
        TextBox4.Text = DataGridView1.Item(3, DataGridView1.CurrentRow.Index).Value
        strRadioText = DataGridView1.Item(4, DataGridView1.CurrentRow.Index).Value
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If DataGridView1.CurrentRow.Index <> DataGridView1.NewRowIndex Then
            Try
                Call bukaDB()
                Dim strRadioText As String
                If RadioButton1.Checked = True Then
                    strRadioText = RadioButton1.Text
                Else
                    strRadioText = RadioButton2.Text
                End If
                ubah = "UPDATE pemilik SET id_pem= '" & TextBox1.Text & "' ,nama = '" & TextBox2.Text & "',alamat = '" & TextBox3.Text & "',nohp = '" & TextBox4.Text & "',status = '" & strRadioText & "' where id_pem = '" & TextBox1.Text & "'"
                CMD = New MySql.Data.MySqlClient.MySqlCommand(ubah, Conn)
                CMD.ExecuteNonQuery()
                Call autoNumber()

                Call tampilPemilik()

                Call clearPemilik()
            Catch ex As Exception
                MsgBox(ex.ToString, MsgBoxStyle.Critical, "Terjadi kesalahan")
            End Try
        End If
    End Sub
End Class