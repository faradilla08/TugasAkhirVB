﻿Imports MySql.Data.MySqlClient
Public Class datakaryawan

    Sub tampilKaryawan()
        Call bukaDB()
        DA = New MySqlDataAdapter("SELECT id_kar 'ID',nama 'Nama',alamat 'Alamat',nohp 'No HP', email 'Email' from karyawan", Conn)
        DS = New DataSet
        DA.Fill(DS, "karyawan")
        DataGridView1.DataSource = DS.Tables("karyawan")
        DataGridView1.ReadOnly = True
    End Sub

    Sub autoNumber()
        Call bukaDB()
        CMD = New MySqlCommand("select * from karyawan order by id_kar desc", Conn)
        RD = CMD.ExecuteReader
        RD.Read()
        If Not RD.HasRows Then
            TextBox1.Text = "4001"
        Else
            TextBox1.Text = Val(Microsoft.VisualBasic.Mid(RD.Item("id_kar").ToString, 4, 3)) + 1
            If Len(TextBox1.Text) = 1 Then
                TextBox1.Text = "400" & TextBox1.Text & ""
            ElseIf Len(TextBox1.Text) = 2 Then
                TextBox1.Text = "40" & TextBox1.Text & ""
            ElseIf Len(TextBox1.Text) = 3 Then
                TextBox1.Text = "4" & TextBox1.Text & ""
            End If
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call autoNumber()
        Call tampilKaryawan()
    End Sub
    Sub clearKaryawan()
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox1.Focus()
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
            Try
                Call bukaDB()
                CMD = New MySqlCommand("SELECT id_kar from karyawan WHERE id_kar = '" & TextBox1.Text & "'", Conn)
                RD = CMD.ExecuteReader
                RD.Read()
                If RD.HasRows Then
                    MsgBox("Maaf Data dengan ID tersebut sudah ada", MsgBoxStyle.Exclamation, "peringatan")
                Else
                    Call bukaDB()
                    simpan = "INSERT INTO karyawan (id_kar,nama,alamat,nohp,email) VALUES (?,?,?,?,?)"
                    CMD = Conn.CreateCommand
                    With CMD
                        .CommandText = simpan
                        .Connection = Conn
                    .Parameters.Add("p1", MySqlDbType.Int16, 100).Value = TextBox1.Text
                        .Parameters.Add("p2", MySqlDbType.VarChar, 100).Value = TextBox2.Text
                        .Parameters.Add("p3", MySqlDbType.VarChar, 100).Value = TextBox3.Text
                        .Parameters.Add("p4", MySqlDbType.VarChar, 100).Value = TextBox4.Text
                        .Parameters.Add("p5", MySqlDbType.VarChar, 100).Value = TextBox5.Text
                        .ExecuteNonQuery()
                    End With
                Call tampilKaryawan()
                Call autoNumber()
                Call clearKaryawan()
                End If
            Catch ex As Exception
                MsgBox(ex.ToString, MsgBoxStyle.Critical, "Terjadi kesalahan")
            End Try
    End Sub

    Private Sub LinkLabel3_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked
        halamanutama.Show()
        Me.Hide()

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        clearKaryawan()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If DataGridView1.CurrentRow.Index <> DataGridView1.NewRowIndex Then
            Dim HapusDetail As String = "delete from karyawan where id_kar=" &
            DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value & ""
            CMD = New MySql.Data.MySqlClient.MySqlCommand(HapusDetail, Conn)
            CMD.ExecuteNonQuery()
            DataGridView1.Rows.RemoveAt(DataGridView1.CurrentRow.Index)
            Call autoNumber()
            Call tampilKaryawan()
            Call clearKaryawan()
        End If
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        TextBox1.Text = DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value
        TextBox2.Text = DataGridView1.Item(1, DataGridView1.CurrentRow.Index).Value
        TextBox3.Text = DataGridView1.Item(2, DataGridView1.CurrentRow.Index).Value
        TextBox4.Text = DataGridView1.Item(3, DataGridView1.CurrentRow.Index).Value
        TextBox5.Text = DataGridView1.Item(4, DataGridView1.CurrentRow.Index).Value
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        TextBox1.Text = DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value
        TextBox2.Text = DataGridView1.Item(1, DataGridView1.CurrentRow.Index).Value
        TextBox3.Text = DataGridView1.Item(2, DataGridView1.CurrentRow.Index).Value
        TextBox4.Text = DataGridView1.Item(3, DataGridView1.CurrentRow.Index).Value
        TextBox5.Text = DataGridView1.Item(4, DataGridView1.CurrentRow.Index).Value
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If DataGridView1.CurrentRow.Index <> DataGridView1.NewRowIndex Then
            Try
                Call bukaDB()
                ubah = "UPDATE karyawan SET id_kar= '" & TextBox1.Text & "' ,nama = '" & TextBox2.Text & "',alamat = '" & TextBox3.Text & "',nohp = '" & TextBox4.Text & "',email = '" & TextBox5.Text & "' where id_kar = '" & TextBox1.Text & "'"
                CMD = New MySql.Data.MySqlClient.MySqlCommand(ubah, Conn)
                CMD.ExecuteNonQuery()
                Call tampilKaryawan()
                Call autoNumber()
                Call clearKaryawan()
            Catch ex As Exception
                MsgBox(ex.ToString, MsgBoxStyle.Critical, "Terjadi kesalahan")
            End Try
        End If
    End Sub
End Class
