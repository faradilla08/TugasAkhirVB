﻿Imports MySql.Data.MySqlClient
Public Class datahewan

    Sub tampilHewan()
        Call bukaDB()
        DA = New MySqlDataAdapter("SELECT id_hewan 'ID',nama 'Nama',biaya 'Biaya' from hewan", Conn)
        DS = New DataSet
        DA.Fill(DS, "hewan")
        DataGridView1.DataSource = DS.Tables("hewan")
        DataGridView1.ReadOnly = True
    End Sub
    Sub autoNumber()
        Call bukaDB()
        CMD = New MySqlCommand("select * from hewan order by id_hewan desc", Conn)
        RD = CMD.ExecuteReader
        RD.Read()
        If Not RD.HasRows Then
            TextBox1.Text = "3001"
        Else
            TextBox1.Text = Val(Microsoft.VisualBasic.Mid(RD.Item("id_hewan").ToString, 4, 3)) + 1
            If Len(TextBox1.Text) = 1 Then
                TextBox1.Text = "300" & TextBox1.Text & ""
            ElseIf Len(TextBox1.Text) = 2 Then
                TextBox1.Text = "30" & TextBox1.Text & ""
            ElseIf Len(TextBox1.Text) = 3 Then
                TextBox1.Text = "3" & TextBox1.Text & ""
            End If
        End If
    End Sub
    Private Sub datahewan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call autoNumber()

        Call tampilHewan()
    End Sub
    Sub clearHewan()
        TextBox2.Text = ""
        TextBox5.Text = ""
        TextBox1.Focus()
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
            Try
                Call bukaDB()
                CMD = New MySqlCommand("SELECT id_hewan from hewan WHERE id_hewan = '" & TextBox1.Text & "'", Conn)
                RD = CMD.ExecuteReader
                RD.Read()
                If RD.HasRows Then
                    MsgBox("Maaf Data dengan ID tersebut sudah ada", MsgBoxStyle.Exclamation, "peringatan")
                Else
                    Call bukaDB()
                simpan = "INSERT INTO hewan (id_hewan,nama,biaya) VALUES (?,?,?)"
                    CMD = Conn.CreateCommand
                    With CMD
                        .CommandText = simpan
                        .Connection = Conn
                    .Parameters.Add("p1", MySqlDbType.Int16, 100).Value = TextBox1.Text
                        .Parameters.Add("p2", MySqlDbType.VarChar, 100).Value = TextBox2.Text
                        .Parameters.Add("p5", MySqlDbType.VarChar, 100).Value = TextBox5.Text
                        .ExecuteNonQuery()
                End With
                Call autoNumber()
                    Call tampilHewan()
                    Call clearHewan()
                End If
            Catch ex As Exception
                MsgBox(ex.ToString, MsgBoxStyle.Critical, "Terjadi kesalahan")
            End Try
    End Sub

  
    Private Sub LinkLabel3_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked
        halamanutama.Show()
        Me.Hide()
    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Call clearHewan()

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If DataGridView1.CurrentRow.Index <> DataGridView1.NewRowIndex Then
            Dim HapusDetail As String = "delete from hewan where id_hewan=" &
            DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value & ""
            CMD = New MySql.Data.MySqlClient.MySqlCommand(HapusDetail, Conn)
            CMD.ExecuteNonQuery()
            DataGridView1.Rows.RemoveAt(DataGridView1.CurrentRow.Index)
            Call autoNumber()
            Call tampilHewan()
            Call clearHewan()
        End If
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        TextBox1.Text = DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value
        TextBox2.Text = DataGridView1.Item(1, DataGridView1.CurrentRow.Index).Value
        TextBox5.Text = DataGridView1.Item(2, DataGridView1.CurrentRow.Index).Value
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        TextBox1.Text = DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value
        TextBox2.Text = DataGridView1.Item(1, DataGridView1.CurrentRow.Index).Value
        TextBox5.Text = DataGridView1.Item(2, DataGridView1.CurrentRow.Index).Value
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If DataGridView1.CurrentRow.Index <> DataGridView1.NewRowIndex Then
            Try
                Call bukaDB()
                ubah = "UPDATE hewan SET id_hewan= '" & TextBox1.Text & "' ,nama = '" & TextBox2.Text & "',biaya = '" & TextBox5.Text & "' where id_hewan = '" & TextBox1.Text & "'"
                CMD = New MySql.Data.MySqlClient.MySqlCommand(ubah, Conn)
                CMD.ExecuteNonQuery()
                Call autoNumber()
                Call tampilHewan()
                Call clearHewan()
            Catch ex As Exception
                MsgBox(ex.ToString, MsgBoxStyle.Critical, "Terjadi kesalahan")
            End Try
        End If
    End Sub



    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
       Button1.Visible = False
        PrintForm1.PrintAction = Printing.PrintAction.PrintToPreview
        PrintForm1.Print()
        Button1.Visible = True
    End Sub
End Class