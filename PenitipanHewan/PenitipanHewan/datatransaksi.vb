﻿Imports MySql.Data.MySqlClient
Public Class datatransaksi

    Sub tampilTransaksi()
        Call bukaDB()
        DA = New MySqlDataAdapter("SELECT id_trans 'ID',tgl_skrg 'Tanggal',nama_pem 'Pemilik',status 'Status',nama_hewan 'Hewan', biaya 'Biaya', berapahari 'Jml Hari', total 'Total',dibayar 'Dibayar',kembali 'Kembali' from transaksi", Conn)
        DS = New DataSet
        DA.Fill(DS, "transaksi")
        DataGridView1.DataSource = DS.Tables("transaksi")
        DataGridView1.ReadOnly = True
    End Sub
    Sub autoNumber()
        Call bukaDB()
        CMD = New MySqlCommand("select * from transaksi order by id_trans desc", Conn)
        RD = CMD.ExecuteReader
        RD.Read()
        If Not RD.HasRows Then
            TextBox1.Text = "0001"
        Else
            TextBox1.Text = Val(Microsoft.VisualBasic.Mid(RD.Item("id_trans").ToString, 4, 3)) + 1
            If Len(TextBox1.Text) = 1 Then
                TextBox1.Text = "500" & TextBox1.Text & ""
            ElseIf Len(TextBox1.Text) = 2 Then
                TextBox1.Text = "50" & TextBox1.Text & ""
            ElseIf Len(TextBox1.Text) = 3 Then
                TextBox1.Text = "5" & TextBox1.Text & ""
            End If
        End If
    End Sub
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call autoNumber()
        Call tampilTransaksi()
        TextBox2.Text = Today
    End Sub
    Sub tampilHewan()

        Try

            Call bukaDB()

            Dim str As String

            str = "select * from hewan where nama = '" & TextBox5.Text & "'"

            CMD = New MySqlCommand(str, Conn)

            RD = CMD.ExecuteReader

            RD.Read()

            If RD.HasRows Then

                TextBox6.Text = RD.Item("biaya")

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Sub tampilPemilik()

        Try

            Call bukaDB()

            Dim str As String

            str = "select * from pemilik where nama = '" & TextBox3.Text & "'"

            CMD = New MySqlCommand(str, Conn)

            RD = CMD.ExecuteReader

            RD.Read()

            If RD.HasRows Then

                TextBox4.Text = RD.Item("status")

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Sub clearTransaksi()
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
        TextBox8.Text = ""
        TextBox9.Text = ""
        TextBox10.Text = ""
        TextBox1.Focus()
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Call bukaDB()
            CMD = New MySqlCommand("SELECT id_trans from transaksi WHERE id_trans = '" & TextBox1.Text & "'", Conn)
            RD = CMD.ExecuteReader
            RD.Read()
            If RD.HasRows Then
                MsgBox("Maaf Data dengan ID tersebut sudah ada", MsgBoxStyle.Exclamation, "peringatan")
            Else
                Call bukaDB()
                simpan = "INSERT INTO transaksi (id_trans,tgl_skrg,nama_pem,status,nama_hewan,biaya,berapahari,total,dibayar,kembali) VALUES (?,?,?,?,?,?,?,?,?,?)"
                CMD = Conn.CreateCommand
                With CMD
                    .CommandText = simpan
                    .Connection = Conn
                    .Parameters.Add("p1", MySqlDbType.Int16, 100).Value = TextBox1.Text
                    .Parameters.Add("p2", MySqlDbType.VarChar, 100).Value = TextBox2.Text
                    .Parameters.Add("p3", MySqlDbType.VarChar, 100).Value = TextBox3.Text
                    .Parameters.Add("p4", MySqlDbType.VarChar, 100).Value = TextBox4.Text
                    .Parameters.Add("p5", MySqlDbType.VarChar, 100).Value = TextBox5.Text
                    .Parameters.Add("p6", MySqlDbType.Int16, 11).Value = TextBox6.Text
                    .Parameters.Add("p7", MySqlDbType.Int16, 11).Value = TextBox7.Text
                    .Parameters.Add("p8", MySqlDbType.Int16, 11).Value = TextBox8.Text
                    .Parameters.Add("p9", MySqlDbType.Int16, 11).Value = TextBox9.Text
                    .Parameters.Add("p10", MySqlDbType.Int16, 11).Value = TextBox10.Text
                    .ExecuteNonQuery()
                End With
                Call tampilTransaksi()
                Call clearTransaksi()
                Call autoNumber()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Terjadi kesalahan")
        End Try
    End Sub

    Private Sub LinkLabel3_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked
        halamanutama.Show()
        Me.Hide()

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Call clearTransaksi()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If DataGridView1.CurrentRow.Index <> DataGridView1.NewRowIndex Then
            Dim HapusDetail As String = "delete from transaksi where id_trans=" &
            DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value & ""
            CMD = New MySql.Data.MySqlClient.MySqlCommand(HapusDetail, Conn)
            CMD.ExecuteNonQuery()
            DataGridView1.Rows.RemoveAt(DataGridView1.CurrentRow.Index)
            Call autoNumber()
            Call tampilTransaksi()
            Call clearTransaksi()
        End If
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        TextBox1.Text = DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value
        TextBox2.Text = DataGridView1.Item(1, DataGridView1.CurrentRow.Index).Value
        TextBox3.Text = DataGridView1.Item(2, DataGridView1.CurrentRow.Index).Value
        TextBox4.Text = DataGridView1.Item(3, DataGridView1.CurrentRow.Index).Value
        TextBox5.Text = DataGridView1.Item(4, DataGridView1.CurrentRow.Index).Value
        TextBox6.Text = DataGridView1.Item(5, DataGridView1.CurrentRow.Index).Value
        TextBox7.Text = DataGridView1.Item(6, DataGridView1.CurrentRow.Index).Value
        TextBox8.Text = DataGridView1.Item(7, DataGridView1.CurrentRow.Index).Value
        TextBox9.Text = DataGridView1.Item(8, DataGridView1.CurrentRow.Index).Value
        TextBox10.Text = DataGridView1.Item(9, DataGridView1.CurrentRow.Index).Value
        nota.TextBox1.Text = DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value
        nota.TextBox2.Text = DataGridView1.Item(1, DataGridView1.CurrentRow.Index).Value
        nota.TextBox3.Text = DataGridView1.Item(2, DataGridView1.CurrentRow.Index).Value
        nota.TextBox4.Text = DataGridView1.Item(3, DataGridView1.CurrentRow.Index).Value
        nota.TextBox5.Text = DataGridView1.Item(4, DataGridView1.CurrentRow.Index).Value
        nota.TextBox6.Text = DataGridView1.Item(5, DataGridView1.CurrentRow.Index).Value
        nota.TextBox7.Text = DataGridView1.Item(6, DataGridView1.CurrentRow.Index).Value
        nota.TextBox8.Text = DataGridView1.Item(7, DataGridView1.CurrentRow.Index).Value
        nota.TextBox9.Text = DataGridView1.Item(8, DataGridView1.CurrentRow.Index).Value
        nota.TextBox10.Text = DataGridView1.Item(9, DataGridView1.CurrentRow.Index).Value
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        TextBox1.Text = DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value
        TextBox2.Text = DataGridView1.Item(1, DataGridView1.CurrentRow.Index).Value
        TextBox3.Text = DataGridView1.Item(2, DataGridView1.CurrentRow.Index).Value
        TextBox4.Text = DataGridView1.Item(3, DataGridView1.CurrentRow.Index).Value
        TextBox5.Text = DataGridView1.Item(4, DataGridView1.CurrentRow.Index).Value
        TextBox6.Text = DataGridView1.Item(5, DataGridView1.CurrentRow.Index).Value
        TextBox7.Text = DataGridView1.Item(6, DataGridView1.CurrentRow.Index).Value
        TextBox8.Text = DataGridView1.Item(7, DataGridView1.CurrentRow.Index).Value
        TextBox9.Text = DataGridView1.Item(8, DataGridView1.CurrentRow.Index).Value
        TextBox10.Text = DataGridView1.Item(9, DataGridView1.CurrentRow.Index).Value
        nota.TextBox1.Text = DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value
        nota.TextBox2.Text = DataGridView1.Item(1, DataGridView1.CurrentRow.Index).Value
        nota.TextBox3.Text = DataGridView1.Item(2, DataGridView1.CurrentRow.Index).Value
        nota.TextBox4.Text = DataGridView1.Item(3, DataGridView1.CurrentRow.Index).Value
        nota.TextBox5.Text = DataGridView1.Item(4, DataGridView1.CurrentRow.Index).Value
        nota.TextBox6.Text = DataGridView1.Item(5, DataGridView1.CurrentRow.Index).Value
        nota.TextBox7.Text = DataGridView1.Item(6, DataGridView1.CurrentRow.Index).Value
        nota.TextBox8.Text = DataGridView1.Item(7, DataGridView1.CurrentRow.Index).Value
        nota.TextBox9.Text = DataGridView1.Item(8, DataGridView1.CurrentRow.Index).Value
        nota.TextBox10.Text = DataGridView1.Item(9, DataGridView1.CurrentRow.Index).Value
    End Sub

   

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If DataGridView1.CurrentRow.Index <> DataGridView1.NewRowIndex Then
            Try
                Call bukaDB()
                ubah = "UPDATE transaksi SET id_trans= '" & TextBox1.Text & "' ,tgl_skrg = '" & TextBox2.Text & "',nama_pem = '" & TextBox3.Text & "',status = '" & TextBox4.Text & "',nama_hewan = '" & TextBox5.Text & "',biaya = '" & TextBox6.Text & "',berapahari = '" & TextBox7.Text & "',total = '" & TextBox8.Text & "',dibayar = '" & TextBox9.Text & "',kembali = '" & TextBox10.Text & "' where id_trans = '" & TextBox1.Text & "'"
                CMD = New MySql.Data.MySqlClient.MySqlCommand(ubah, Conn)
                CMD.ExecuteNonQuery()
                Call tampilTransaksi()
                Call clearTransaksi()
                Call autoNumber()
            Catch ex As Exception
                MsgBox(ex.ToString, MsgBoxStyle.Critical, "Terjadi kesalahan")
            End Try
        End If
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If TextBox4.Text = "Member" Then
            TextBox8.Text = 95 / 100 * (Val(TextBox6.Text) * Val(TextBox7.Text))
        ElseIf TextBox4.Text = "Non Member" Then
            TextBox8.Text = Val(TextBox6.Text) * Val(TextBox7.Text)
        End If
        TextBox10.Text = Val(TextBox9.Text) - Val(TextBox8.Text)
    End Sub

    Private Sub TextBox3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox3.KeyPress
        tampilPemilik()
    End Sub

    Private Sub TextBox5_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox5.KeyPress
        tampilHewan()
    End Sub

    Private Sub TextBox7_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox7.KeyPress
        If TextBox4.Text = "Member" Then
            TextBox8.Text = 95 / 100 * (Val(TextBox6.Text) * Val(TextBox7.Text))
        ElseIf TextBox4.Text = "Non Member" Then
            TextBox8.Text = Val(TextBox6.Text) * Val(TextBox7.Text)
        End If
    End Sub
    Private Sub TextBox9_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox9.KeyPress
        TextBox10.Text = Val(TextBox9.Text) - Val(TextBox8.Text)
    End Sub

End Class
